const {Buffer} = require("buffer")
const express = require("express");
const axios = require("axios");
const app = express();
let url = "https://monkey.banano.cc/api/v1/monkey/";
let monKeyMap= new Map();

function isValidAddress(address)
{
	return ((address.substring(0,5) == 'ban_1' || address.substring(0,5) == 'ban_3') && address.length == 64)
}
app.use(function (request, response, next) {
	response.set('Cache-control', 'public, max-age=300')
	next()
})
app.get("/:address", async(request, response) => 
{
	let address = request.params.address
	let status = 200
	if(!request.params)
	{
    	return response.send("NO PARAMS PASSED")
	}
	if(address == undefined)
	{	
		return response.send("not ban address")
	}
	if(!isValidAddress(address))
	{	
		return response.send("not ban address")
	}
	
	url = url+address;
	let monkeyIMG;

	if(monKeyMap.get(address) != undefined)
	{	
		monkeyIMG = monKeyMap.get(address);
	}
	else
	{
		
		try
		{
			
			const arrayBuffer = await axios
			.get("https://monkey.banano.cc/api/v1/monkey/"+address+"?format=png&size=256", {
				responseType: 'arraybuffer'
			})
			.catch(function (error) 
			{
				status = error.response.status
			})
			if(status == 200)
			{
				let buffer = Buffer.from(arrayBuffer.data, 'base64');
				// let image = `data:${arrayBuffer.headers["content-type"]};base64,${buffer}`;
				monKeyMap.set(address, buffer)
				monkeyIMG = monKeyMap.get(address)
			}
		}
		catch (err)
		{
			console.log('err', err)
		}
	}
	try
	{
		if(status == 200)
		{
	    	response.writeHead(200, {
				'Content-Type': 'image/png',
				'Content-Length': (monkeyIMG == undefined) ? 0 : monkeyIMG.length
			  });
			response.end(monkeyIMG); 
		}
		else
		{
			response.status(400).send("Invalid Address")
		}
	}
	catch(err)
	{
		console.log(err)
	}
});

app.listen(18888, '127.0.0.1', () => console.log("Server is up and running.."));